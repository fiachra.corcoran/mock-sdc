package main

import (
	"net/http"

	"github.com/labstack/echo"
)

func index(c echo.Context) error {
	return c.String(http.StatusOK, "Hello, World!")
}

func reset(c echo.Context) error {
	generateInitialVendorList()
	generateInitialVspList()
	return c.String(http.StatusCreated, "reset done!")
}
