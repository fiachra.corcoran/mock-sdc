package main

import (
	"net/http"

	"github.com/labstack/echo"
)

// VersionList describe the list return in SDC
type VersionList struct {
	ListCount int            `json:"listCount`
	Results   []VersionLight `json:"results"`
}

// Version describe version model in SDC
type Version struct {
	ID               string `json:"id"`
	Name             string `json:"name"`
	Description      string `json:"description"`
	BaseID           string `json:"baseId"`
	Status           string `json:"status"`
	RealStatus       string
	CreationTime     int64 `json:"creationTime"`
	ModificationTime int64 `json:"modificationTime"`
	AdditionalInfo   struct {
		OptionalCreationMethods []string `json:"OptionalCreationMethods"`
	} `json:"additionalInfo"`
	State struct {
		SynchronizationState string `json:"synchronizationState"`
		Dirty                bool   `json:"dirty"`
	} `json:"state"`
}

// VersionLight describe version model in SDC
type VersionLight struct {
	ID               string `json:"id"`
	Name             string `json:"name"`
	Description      string `json:"description"`
	BaseID           string `json:"baseId"`
	Status           string `json:"status"`
	CreationTime     int64  `json:"creationTime"`
	ModificationTime int64  `json:"modificationTime"`
	AdditionalInfo   struct {
		OptionalCreationMethods []string `json:"OptionalCreationMethods"`
	} `json:"additionalInfo"`
}

// VersionDetails show details of a version
type VersionDetails struct {
	ID               string `json:"id"`
	Name             string `json:"name"`
	Description      string `json:"description"`
	Status           string `json:"status"`
	CreationTime     int64  `json:"creationTime"`
	ModificationTime int64  `json:"modificationTime"`
	State            struct {
		SynchronizationState string `json:"synchronizationState"`
		Dirty                bool   `json:"dirty"`
	} `json:"state"`
}

// CreatedVersion of a CreatedVendor in SDC
type CreatedVersion struct {
	ID          string `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
	Status      string `json:"status"`
}

// CreatedItem model in SDC
type CreatedItem struct {
	ItemID  string         `json:"itemId"`
	Version CreatedVersion `json:"version"`
}

// Action describe the action on items in SDC
type Action struct {
	Action string `json:"action"`
}

// SdcError is the way to return Error in SDC
type SdcError struct {
	Status    string `json:"status"`
	ErrorCode string `json:"errorCode"`
	Message   string `json:"message"`
}

func getItemVersions(c echo.Context) error {
	itemID := c.Param("itemID")
	for _, v := range vendorList {
		if v.ID == itemID {
			versionList := generateVersionList(v.Versions)
			list := &VersionList{len(versionList), versionList}
			return c.JSON(http.StatusOK, list)
		}
	}
	for _, v := range vspList {
		if v.ID == itemID {
			versionList := generateVersionList(v.Versions)
			list := &VersionList{len(versionList), versionList}
			return c.JSON(http.StatusOK, list)
		}
	}
	return echo.NewHTTPError(http.StatusNotFound, "Item Not Found")
}

func generateVersionList(versions []Version) []VersionLight {
	versionList := []VersionLight{}
	for _, version := range versions {
		versionList = append(versionList, VersionLight{
			ID:               version.ID,
			Name:             version.Name,
			Description:      version.Description,
			BaseID:           version.BaseID,
			Status:           version.Status,
			CreationTime:     version.CreationTime,
			ModificationTime: version.ModificationTime,
			AdditionalInfo:   version.AdditionalInfo,
		})

	}
	return versionList
}

func getItemVersion(c echo.Context) error {
	itemID := c.Param("itemID")
	versionID := c.Param("versionID")
	for _, v := range vendorList {
		if v.ID == itemID {
			for _, version := range v.Versions {
				if version.ID == versionID {
					versionDetails := VersionDetails{
						ID:               version.ID,
						Name:             version.Name,
						Description:      version.Description,
						Status:           version.Status,
						CreationTime:     version.CreationTime,
						ModificationTime: version.ModificationTime,
						State:            version.State,
					}
					return c.JSON(http.StatusOK, versionDetails)
				}
			}
		}
	}
	for _, v := range vspList {
		if v.ID == itemID {
			for _, version := range v.Versions {
				if version.ID == versionID {
					versionDetails := VersionDetails{
						ID:               version.ID,
						Name:             version.Name,
						Description:      version.Description,
						Status:           version.Status,
						CreationTime:     version.CreationTime,
						ModificationTime: version.ModificationTime,
						State:            version.State,
					}
					return c.JSON(http.StatusOK, versionDetails)
				}
			}
		}
	}
	return echo.NewHTTPError(http.StatusNotFound, "Item Not Found")
}

func updateItemVersion(c echo.Context) error {
	action := new(Action)
	if err := c.Bind(action); err != nil {
		return err
	}
	itemID := c.Param("itemID")
	versionID := c.Param("versionID")
	for i, v := range vspList {
		if v.ID == itemID {
			for j, version := range v.Versions {
				if version.ID == versionID {
					if action.Action == "Commit" {
						if version.RealStatus == "Validated" {
							vspList[i].Versions[j].RealStatus = "Commited"
							vspList[i].Versions[j].State.Dirty = false
							return c.String(http.StatusOK, "{}")
						}
						return echo.NewHTTPError(http.StatusNotFound, "Item not in good state")
					}
					return echo.NewHTTPError(http.StatusNotFound, "Unknown Action")
				}
			}
		}
	}
	return echo.NewHTTPError(http.StatusNotFound, "Item Not Found")
}
